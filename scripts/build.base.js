const { NodeModulesPolyfillPlugin } = require('@esbuild-plugins/node-modules-polyfill');
const esbuild = require('esbuild');

module.exports = options =>
  esbuild
    .build({
      bundle: true,
      plugins: [NodeModulesPolyfillPlugin()],
      loader: {
        '.html': 'text',
      },
      external: ['vscode'],
      ...options,
    })
    .catch(() => process.exit(1));
