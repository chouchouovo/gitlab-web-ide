import { GitLabClient } from '@gitlab/gitlab-api-client';
import { ICommand, VSBufferWrapper } from '../types';
import {
  createCommands,
  COMMAND_START,
  COMMAND_FETCH_FILE_RAW,
  COMMAND_READY,
  COMMAND_START_REMOTE,
  COMMAND_OPEN_URI,
  COMMAND_COMMIT,
  COMMAND_PREVENT_UNLOAD,
  COMMAND_SET_HREF,
  COMMAND_TRACK_EVENT,
  COMMAND_FETCH_PROJECT_BRANCHES,
  COMMAND_CREATE_PROJECT_BRANCH,
} from './index';
import * as commit from './commit';
import * as start from './start';
import * as fetchFileRaw from './fetchFileRaw';
import { postMessage } from './utils/postMessage';
import { TEST_COMMIT_PAYLOAD, TEST_CONFIG, createMockLocation } from '../../test-utils';

jest.mock('@gitlab/gitlab-api-client');
jest.mock('./commit');
jest.mock('./start');
jest.mock('./fetchFileRaw');
jest.mock('./utils/postMessage');

const TEST_BUFFER_WRAPPER: VSBufferWrapper = () => ({
  buffer: new Uint8Array(),
});

describe('vscode-mediator-commands/commands', () => {
  let commitSpy: jest.Mock;
  let startSpy: jest.Mock;
  let fetchFileRawSpy: jest.Mock;
  let commands: ICommand[] = [];

  const callCommand = (id: string, ...args: unknown[]) => {
    commands.find(x => x.id === id)?.handler(...args);
  };

  const createCommandSpy = (commandFactoryModule: { commandFactory: unknown }) => {
    const spy = jest.fn();

    (commandFactoryModule.commandFactory as jest.Mock).mockImplementation(
      (...dependencies: unknown[]) =>
        (...args: unknown[]) =>
          spy({
            dependencies,
            args,
          }),
    );

    return spy;
  };

  const getClient = () => jest.mocked(GitLabClient).mock.instances[0];

  beforeEach(() => {
    // createCommandSpy sets up the module mock too, so we have to call this before createCommands
    commitSpy = createCommandSpy(commit);
    startSpy = createCommandSpy(start);
    fetchFileRawSpy = createCommandSpy(fetchFileRaw);

    commands = createCommands(TEST_CONFIG, TEST_BUFFER_WRAPPER);
  });

  describe(COMMAND_START, () => {
    beforeEach(() => {
      callCommand(COMMAND_START, 'arg');
    });

    it('triggers start command', () => {
      expect(startSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient)],
        args: ['arg'],
      });
    });
  });

  describe(COMMAND_FETCH_FILE_RAW, () => {
    beforeEach(() => {
      callCommand(COMMAND_FETCH_FILE_RAW, 'ref', 'path');
    });

    it('triggers fetchFileRaw command', () => {
      expect(fetchFileRawSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient), TEST_BUFFER_WRAPPER],
        args: ['ref', 'path'],
      });
    });
  });

  describe.each<[string, keyof GitLabClient, unknown]>([
    [
      COMMAND_FETCH_PROJECT_BRANCHES,
      'fetchProjectBranches',
      { projectPath: 'lorem/ipsum', searchPattern: '*foo*', offset: 0, limit: 100 },
    ],
    [
      COMMAND_CREATE_PROJECT_BRANCH,
      'createProjectBranch',
      { projectPath: 'lorem/ipsum', searchPattern: '*foo*', offset: 0, limit: 100 },
    ],
  ])('%s', (command, clientMethodName, payload) => {
    it('calls client with payload', () => {
      callCommand(command, payload);

      expect(getClient()[clientMethodName]).toHaveBeenCalledWith(payload);
    });
  });

  describe(COMMAND_READY, () => {
    beforeEach(() => {
      callCommand(COMMAND_READY);
    });

    it('triggers postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({ key: 'ready' });
    });
  });

  describe(COMMAND_START_REMOTE, () => {
    beforeEach(() => {
      callCommand(COMMAND_START_REMOTE, { connectionToken: '123' });
    });

    it('sends start-message postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'start-remote',
        params: { connectionToken: '123' },
      });
    });
  });

  describe(COMMAND_OPEN_URI, () => {
    beforeEach(() => {
      window.open = jest.fn();
    });

    it.each`
      key                  | uriValue
      ${'feedbackIssue'}   | ${TEST_CONFIG.links.feedbackIssue}
      ${'userPreferences'} | ${TEST_CONFIG.links.userPreferences}
    `('opens $uriValue in new window when uri key is $key', ({ key, uriValue }) => {
      callCommand(COMMAND_OPEN_URI, { key });

      expect(window.parent.open).toHaveBeenCalledWith(uriValue, '_blank', 'noopener,noreferrer');
    });
  });

  describe(COMMAND_COMMIT, () => {
    beforeEach(() => {
      callCommand(COMMAND_COMMIT, TEST_COMMIT_PAYLOAD);
    });

    it('triggers commit command', () => {
      expect(commitSpy).toHaveBeenCalledWith({
        dependencies: [TEST_CONFIG, expect.any(GitLabClient)],
        args: [TEST_COMMIT_PAYLOAD],
      });
    });
  });

  describe(COMMAND_PREVENT_UNLOAD, () => {
    beforeEach(() => {
      callCommand(COMMAND_PREVENT_UNLOAD, { shouldPrevent: true });
    });

    it('sends start-message postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'prevent-unload',
        params: { shouldPrevent: true },
      });
    });
  });

  describe(COMMAND_TRACK_EVENT, () => {
    beforeEach(() => {
      callCommand(COMMAND_TRACK_EVENT, [{ name: 'connect-remote' }]);
    });

    it('sends web-ide-tracking postMessage', () => {
      expect(postMessage).toHaveBeenCalledWith({
        key: 'web-ide-tracking',
        params: [{ name: 'connect-remote' }],
      });
    });
  });

  describe(COMMAND_SET_HREF, () => {
    const origWindowParent = window.parent;

    beforeEach(() => {
      window.parent = { ...window };
      const mockLocation = createMockLocation();
      Object.defineProperty(window.parent, 'location', {
        get() {
          return mockLocation;
        },
      });
    });

    afterEach(() => {
      window.parent = origWindowParent;
    });

    it.each`
      origHref                    | href                                   | expectHref
      ${'http://localhost/'}      | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'/new/path/place'}                   | ${'http://localhost/new/path/place'}
      ${'http://localhost/-/ide'} | ${'http://example.org/new/path/place'} | ${'http://example.org/new/path/place'}
    `(
      'with origHref=$origHref and href=$href, updates window.parent.location',
      ({ origHref, href, expectHref }) => {
        window.parent.location.href = origHref;

        callCommand(COMMAND_SET_HREF, href);

        expect(window.parent.location.href).toBe(expectHref);
      },
    );
  });
});
