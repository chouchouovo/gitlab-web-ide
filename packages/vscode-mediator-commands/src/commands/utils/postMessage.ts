import { Message } from '@gitlab/web-ide-types';

export const postMessage = (message: Message) => {
  window.postMessage(message, window.location.origin);
};
