import { gitlab, GitLabClient } from '@gitlab/gitlab-api-client';
import { IFullConfig } from '../src/types';

export * from './createMockLocation';

export const TEST_DEFAULT_BRANCH = 'test-default-branch';

export const TEST_CONFIG: IFullConfig = {
  baseUrl: '/test/baseUrl',
  gitlabToken: 'test-gitlabToken',
  gitlabUrl: 'https://example.com/gitlabUrl',
  projectPath: 'lorem/ipsum',
  ref: 'test-main',
  repoRoot: '/repo/root/lorem/ipsum',
  links: {
    feedbackIssue: 'foobar',
    userPreferences: 'user/preferences',
  },
};

export const TEST_COMMIT_PAYLOAD: gitlab.CommitPayload = {
  actions: [],
  branch: 'main-patch-123',
  commit_message: 'Test! Hello!',
};

export const TEST_COMMIT_ID = '000000111111';

export const createTestClient = () =>
  new GitLabClient({ baseUrl: TEST_CONFIG.baseUrl, authToken: TEST_CONFIG.gitlabToken });

export const createTestProject = (projectPath: string): gitlab.Project => ({
  id: 7,
  name: 'My Project',
  path_with_namespace: projectPath,
  web_url: `https://example.com/${projectPath}`,
  default_branch: TEST_DEFAULT_BRANCH,
  can_create_merge_request_in: true,
  empty_repo: false,
});

export const createTestBranch = (projectPath: string, ref: string): gitlab.Branch => ({
  name: ref,
  web_url: `https://example.com/${projectPath}/${ref}`,
  commit: {
    created_at: '',
    id: TEST_COMMIT_ID,
    message: 'Hello! Test!',
    short_id: '000000',
    title: 'Hello! Test!',
    web_url: `https://example.com/${projectPath}/commits/000000`,
  },
});

export const createRepositoryTreeItem = (
  path: string,
  type: 'blob' | 'tree',
): gitlab.RepositoryTreeItem => ({
  name: path,
  id: path,
  path,
  type,
  mode: '100644',
});
