import * as vscode from 'vscode';
import { RELOAD_COMMAND_ID } from '../constants';
import { registerReloadCommand } from './reload';

const TEST_REF = 'new-branch-patch-123';

describe('commands/reload', () => {
  let disposables: jest.MockedObject<vscode.Disposable>[];
  let reloadFn: jest.Mock<Promise<void>, []>;
  let registerCommandSpy: jest.SpyInstance<
    vscode.Disposable,
    [command: string, callback: (...args: unknown[]) => unknown, thisArg?: unknown]
  >;

  const executeCommand = ({ ref = '' }) => registerCommandSpy.mock.calls[0][1]({ ref });

  beforeEach(() => {
    disposables = [];
    reloadFn = jest.fn().mockImplementation(() => Promise.resolve());

    registerCommandSpy = jest
      .spyOn(vscode.commands, 'registerCommand')
      // Note: registerCommand has to return a Disposable, but we also add the commandName
      //       so that we can easily assert what kind of disposable was added to the array
      .mockImplementation(commandName => ({ commandName, dispose: jest.fn() }));
  });

  describe('registerReloadCommand', () => {
    beforeEach(() => {
      registerReloadCommand(disposables, reloadFn);
    });

    it('command is registered and added to disposables', () => {
      expect(registerCommandSpy).toHaveBeenCalledTimes(1);
      expect(registerCommandSpy).toHaveBeenCalledWith(RELOAD_COMMAND_ID, expect.any(Function));
      expect(disposables).toEqual([
        { commandName: RELOAD_COMMAND_ID, dispose: expect.any(Function) },
      ]);
    });

    it('does not call withProgress', () => {
      expect(vscode.window.withProgress).not.toHaveBeenCalled();
    });

    describe('when command is executed', () => {
      let originalDisposable: jest.MockedObject<vscode.Disposable>;
      let otherDisposables: jest.MockedObject<vscode.Disposable>[];

      beforeEach(() => {
        [originalDisposable] = disposables;
        otherDisposables = [{ dispose: jest.fn() }, { dispose: jest.fn() }, { dispose: jest.fn() }];
        disposables.push(...otherDisposables);
        executeCommand({ ref: TEST_REF });
      });

      it('calls dispose on everything except self', () => {
        expect(originalDisposable.dispose).not.toHaveBeenCalled();

        expect(otherDisposables.map(x => x.dispose.mock.calls.length)).toEqual(
          otherDisposables.map(() => 1),
        );
      });

      it('clears out disposables of everything but own disposable', () => {
        expect(disposables).toEqual([
          { commandName: RELOAD_COMMAND_ID, dispose: expect.any(Function) },
        ]);
      });

      it('creates vscode progress bar', () => {
        expect(vscode.window.withProgress).toHaveBeenCalledTimes(1);
        expect(vscode.window.withProgress).toHaveBeenCalledWith(
          {
            cancellable: false,
            location: vscode.ProgressLocation.Notification,
            title: 'Reloading file system...',
          },
          expect.any(Function),
        );
      });

      it('calls reloadFn with progress', () => {
        expect(reloadFn).not.toHaveBeenCalled();

        const [, fn] = (vscode.window.withProgress as jest.Mock).mock.calls[0];
        fn();

        expect(reloadFn).toHaveBeenCalledWith(disposables, { ref: TEST_REF });
      });
    });
  });
});
