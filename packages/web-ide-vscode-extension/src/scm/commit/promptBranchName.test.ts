import * as vscode from 'vscode';
import { generateBranchName } from './generateBranchName';
import { IBranchSelection, promptBranchName } from './promptBranchName';

jest.mock('./generateBranchName');

const TEST_BRANCH = 'test-main';
const TEST_GENERATED_BRANCH = 'test-main-path-123';

describe('scm/commit/promptBranchName', () => {
  let resolveQuickPick: (itemIdx: number) => void;
  let resolveInputBox: (value: string | undefined) => void;
  let result: Promise<IBranchSelection | undefined>;

  beforeEach(() => {
    jest.spyOn(vscode.window, 'showQuickPick').mockImplementation(itemsPromise =>
      Promise.resolve(itemsPromise).then(
        items =>
          new Promise(resolve => {
            resolveQuickPick = idx => resolve(items[idx]);
          }),
      ),
    );

    jest.spyOn(vscode.window, 'showInputBox').mockImplementation(
      () =>
        new Promise(resolve => {
          resolveInputBox = resolve;
        }),
    );

    jest.mocked(generateBranchName).mockReturnValue(TEST_GENERATED_BRANCH);

    result = promptBranchName(TEST_BRANCH);
  });

  it('shows quick pick', () => {
    expect(vscode.window.showQuickPick).toHaveBeenCalledWith(
      [
        { label: 'Yes', description: 'Commit to a new branch' },
        { label: 'No', description: `Use the current branch "${TEST_BRANCH}"` },
      ],
      {
        ignoreFocusOut: true,
        canPickMany: false,
        title: 'Commit to a new branch?',
      },
    );
  });

  it('does not show input box yet', () => {
    expect(vscode.window.showInputBox).not.toHaveBeenCalled();
  });

  describe('is user cancels quick pick', () => {
    beforeEach(() => {
      resolveQuickPick(-1);
    });

    it('resolves to undefined', async () => {
      await expect(result).resolves.toBeUndefined();
    });
  });

  describe('if user selects no', () => {
    beforeEach(() => {
      resolveQuickPick(1);
    });

    it('resolves to given branch', async () => {
      await expect(result).resolves.toEqual({
        isNewBranch: false,
        branchName: TEST_BRANCH,
      });
    });
  });

  describe('if user selects yes', () => {
    beforeEach(() => {
      resolveQuickPick(0);
    });

    it('shows input box', () => {
      expect(vscode.window.showInputBox).toHaveBeenCalledWith({
        ignoreFocusOut: true,
        placeHolder: `Leave empty to use "${TEST_GENERATED_BRANCH}"`,
        title: 'New branch name',
      });
    });

    it.each`
      inputBoxResult        | expectation
      ${undefined}          | ${undefined}
      ${''}                 | ${{ isNewBranch: true, branchName: TEST_GENERATED_BRANCH }}
      ${'brand-new-branch'} | ${{ isNewBranch: true, branchName: 'brand-new-branch' }}
    `(
      'with inputBox resolves with $inputBoxResult, returns $expectation',
      async ({ inputBoxResult, expectation }) => {
        resolveInputBox(inputBoxResult);

        await expect(result).resolves.toEqual(expectation);
      },
    );
  });
});
