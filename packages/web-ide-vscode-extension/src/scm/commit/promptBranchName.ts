import * as vscode from 'vscode';
import { generateBranchName } from './generateBranchName';

export interface IBranchSelection {
  readonly isNewBranch: boolean;
  readonly branchName: string;
}

/**
 * Prompts the user for a branch name and returns a promise resolving
 * with their selection (or undefined if the user canceled).
 */
export const promptBranchName = async (
  currentBranchName: string,
): Promise<IBranchSelection | undefined> => {
  const items = [
    {
      label: 'Yes',
      description: 'Commit to a new branch',
    },
    {
      label: 'No',
      description: `Use the current branch "${currentBranchName}"`,
    },
  ];

  const selection = await vscode.window.showQuickPick(items, {
    ignoreFocusOut: true,
    canPickMany: false,
    title: 'Commit to a new branch?',
  });

  // If we didn't select anything, then cancel the commit
  if (!selection) {
    return undefined;
  }

  if (selection === items[1]) {
    return {
      isNewBranch: false,
      branchName: currentBranchName,
    };
  }

  const generatedBranchName = generateBranchName(currentBranchName);

  const userNewBranchName = await vscode.window.showInputBox({
    ignoreFocusOut: true,
    placeHolder: `Leave empty to use "${generatedBranchName}"`,
    title: 'New branch name',
  });

  // Did the user just escape it?
  if (userNewBranchName === undefined) {
    return undefined;
  }

  return {
    isNewBranch: true,
    branchName: userNewBranchName || generatedBranchName,
  };
};
