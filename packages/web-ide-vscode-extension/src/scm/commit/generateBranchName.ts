// Take the last 4 characters of a random hex
const generateRandomPart = () => Math.random().toString(16).slice(-4);

export const generateBranchName = (branchName: string) =>
  `${branchName}-patch-${generateRandomPart()}`;
